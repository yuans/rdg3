% Author: Dr. Yuan SUN
% email address: yuan.sun@unimelb.edu.au OR suiyuanpku@gmail.com
% ------------
% Description:
% ------------
% grouping - This function is used to read the rdg3 datafiles and
% form the subcomponents which are used by the cooperative co-evolutionary
% framework. This function is called in cmaescc.m.
%
%--------
% Inputs:
%--------
%    fun : the function id for which the corresponding grouping should
%          be loaded by reading the differential grouping datafiles.

function group = grouping(fun)
    filename = sprintf('./rdg3/results/F%02d', fun);
    load(filename);
    
    % further divide the separable subcomponents
    div_sepsize = 100;
    div_num = ceil(length(seps)/div_sepsize);
    div_seps = cell(1, div_num);
    div_startpts = 1:div_sepsize:length(seps);
    for i = 1:div_num-1
       div_seps{i} = seps(div_startpts(i):div_startpts(i+1)-1);
    end
    if (~isempty(seps))
       div_seps{div_num} = seps(div_startpts(div_num):end);
    end
    group = [div_seps nonseps];
end
